<?php

use Illuminate\Database\Seeder;
use App\Models\Role;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name'=>'ADMIN']);//id 1
        Role::create(['name'=>'USER']);//-2
    }
}
