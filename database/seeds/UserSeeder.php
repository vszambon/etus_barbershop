<?php

use Illuminate\Database\Seeder;
use App\Models\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User([
            'name' => 'Josias',
            'email' => 'josias@mail.com.br',
            'password' => Hash::make('gulIOP!@#%RTR',['rounds' => 4]),
            'email_verified_at' => now(),
        ]);
        $user->save();

        $roleId = 1;//ADMIN ROLE
        $user->roles()->attach($roleId);

        $user = new User([
            'name' => 'user',
            'email' => 'user@mail.com.br',
            'password' => Hash::make('gulIOP!@#%RTR',['rounds' => 4]),
            'email_verified_at' => now(),
        ]);
        $user->save();

        $roleId = 2;//USER ROLE
        $user->roles()->attach($roleId);
    }
}
