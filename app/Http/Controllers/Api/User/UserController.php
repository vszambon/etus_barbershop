<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\DefaultCrudControllerImpl;
use App\Http\Controllers\Api\DefaultCrudController;

use Illuminate\Support\Facades\Cache;


class UserController extends Controller implements DefaultCrudController
{
    private $apiController;

    function __construct(){
        $this->apiController = new DefaultCrudControllerImpl('App\Models\User',['roles','phones'],true);
    }

    public function list(Request $request){
        return $this->apiController->list($request);
    }

    function getById($id){
        return $this->apiController->getById($id);
    }

    public function save(Request $request){
        // $this->validateSaveRequest($request);
        // return $this->apiController->save($request);
    }

    public function update(Request $request,$id){
        // $request->only([]); //do not include password
        // $this->validateUpdateRequest($request);
        // return $this->apiController->update($request,$id);
    }

    public function delete($id){
        return $this->apiController->delete($id);
    }


    private function validateSaveRequest(Request $request){
        return $request->validate([

        ]);
    }

    private function validateUpdateRequest(Request $request){
        if(!is_null($request->input('email'))){
            $roles = auth('api')->user()->roles;
            foreach($roles as $role){
                if($role->id != 1 ){
                    $request->request->remove('email');
                }
            }
        }
        return $request->validate([

        ]);
    }

}
