<?php

namespace App\Http\Controllers\Api\Handlers;

use App\Models\Properties\Phone;

class PhoneHandler{

    static private $phones = array();

    function __construct(   ){

    }

    static function handleSaveBefore(&$modelData){
        foreach($modelData['phones'] as $phone){
            $newphone = new Phone();
            $newphone->numero = $phone['numero'];
            array_push(self::$phones,$newphone);
        }
        unset($modelData['phones']);
    }

    static function handleSaveAfter($model){
        $model->phones()->saveMany(self::$phones);
    }

    static function handleUpdateBefore(&$modelData){
        foreach($modelData['phones'] as $phone){
            $newphone = new Phone();
            if(array_key_exists('id',$phone)){
                $newphone->id = $phone['id'];
            }
            $newphone->numero = $phone['numero'];
            array_push(self::$phones,$newphone);
        }
        unset($modelData['phones']);
    }

    static function handleUpdateAfter($model){
        foreach(self::$phones as $phone){
            if(!is_null($phone->id)){
                $model->phones()->where('id','=',$phone->id)->update(['numero'=>$phone->numero]);
            }
            else{
                $model->phones()->save($phone);
            }
        }
    }

}
