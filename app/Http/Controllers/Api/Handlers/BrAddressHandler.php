<?php

namespace App\Http\Controllers\Api\Handlers;

use App\Models\Properties\BrAddress;

class BrAddressHandler{

    static private $addresses = array();

    function __construct(   ){

    }

    static function handleSaveBefore(&$modelData){
        foreach($modelData['br_address'] as $address){
            $newaddress = new BrAddress();
            if(array_key_exists('uf',$address))$newaddress->uf                   = $address['uf'];
            if(array_key_exists('cep',$address))$newaddress->cep                 = $address['cep'];
            if(array_key_exists('bairro',$address))$newaddress->bairro           = $address['bairro'];
            if(array_key_exists('numero',$address))$newaddress->numero           = $address['numero'];
            if(array_key_exists('logradouro',$address))$newaddress->logradouro   = $address['logradouro'];
            if(array_key_exists('localidade',$address))$newaddress->localidade   = $address['localidade'];
            if(array_key_exists('complemento',$address))$newaddress->complemento = $address['complemento'];
            array_push(self::$addresses,$newaddress);
        }
        unset($modelData['br_address']);
    }

    static function handleSaveAfter($model){
        $model->brAddress()->saveMany(self::$addresses);
        return $model;
    }

    static function handleUpdateBefore(&$modelData){
        foreach($modelData['br_address'] as $address){
            $newaddress = new BrAddress();
            if(array_key_exists('id',$address)) {
                $newaddress->id = $address['id'];
            }
            if(array_key_exists('uf',$address))$newaddress->uf                   = $address['uf'];
            if(array_key_exists('cep',$address))$newaddress->cep                 = $address['cep'];
            if(array_key_exists('bairro',$address))$newaddress->bairro           = $address['bairro'];
            if(array_key_exists('numero',$address))$newaddress->numero           = $address['numero'];
            if(array_key_exists('logradouro',$address))$newaddress->logradouro   = $address['logradouro'];
            if(array_key_exists('localidade',$address))$newaddress->localidade   = $address['localidade'];
            if(array_key_exists('complemento',$address))$newaddress->complemento = $address['complemento'];
            array_push(self::$addresses,$newaddress);
        }
        unset($modelData['br_address']);
    }

    static function handleUpdateAfter($model){
        foreach(self::$addresses as $address){
            if(!is_null($address->id)){
                $model->brAddress()->where('id','=',$address->id)->update(
                    [
                        'uf'          => $address->uf,
                        'cep'         => $address->cep,
                        'numero'      => $address->numero,
                        'bairro'      => $address->bairro,
                        'localidade'  => $address->localidade,
                        'logradouro'  => $address->logradouro,
                        'complemento' => $address->complemento,
                    ]
                );
            }
            else{
                $model->brAddress()->save($address);
            }
        }
    }

}
