<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\DefaultCrudController;
use App\Http\Controllers\Api\AuxiliarFunctions;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

use App\Models\Properties\BrAddress;
use App\Models\Properties\Phone;

use App\Http\Controllers\Api\Handlers\BrAddressHandler;
use App\Http\Controllers\Api\Handlers\PhoneHandler;

use Illuminate\Database\Eloquent\Builder;
use DB;

use Illuminate\Support\Facades\Cache;

class DefaultCrudControllerImpl implements DefaultCrudController
{
    public  $model;
    private $modelName;

    private $options;

    private $addToAfterAttribution;
    private $addToBeforeAttribution;

    private $cacheTime;

    function __construct($modelName,$options,$softDeletes=false){
        $this->model       = new $modelName;
        $this->options     = $options;
        $this->modelName   = $modelName;
        $this->cacheTime   = 10;
    }

    public function setAddToAfterAttribution($func){
        $this->addToAfterAttribution = $func;
    }

    public function setAddToBeforeAttribution($func){
        $this->addToBeforeAttribution = $func;
    }

    public function list(Request $request) {

        $request->validate([
            'order'    => 'array',
            'tosearch' => 'array',
        ]);

        $search      = $request['search'];
        $tosearch    = $request['tosearch'] ? implode(',',$request['tosearch']) : null;
        $searchequal = $request['searchequal'];
        $order       = $request['order'] ? implode(',',$request['order']) : null;
        $orderby     = $request['order_by'];
        $perpage     = $request['per_page'];
        $page        = $request['page'];

        return Cache::remember(get_class($this).__FUNCTION__
                                .$search
                                .$tosearch
                                .$searchequal
                                .$order
                                .$orderby
                                .$perpage
                                .$page, $this->cacheTime, function () use($request){

            $model = new $this->modelName;

            $query = $model->query();
    
            if(sizeof($this->options) != 0){
                foreach($this->options as $option){
                    $query->with($option);
                }
            }
    
            if(!is_null($request['search'])){
                if($request['tosearch']){
                    foreach($request['tosearch'] as $column){
                        $query->orWhere($column,'LIKE', '%' . $request['search'] . '%');
                    }
                }
            }
            else if(!is_null($request['searchequal'])){
                if($request['tosearch']){
                    foreach($request['tosearch'] as $column){
                        $query->orWhere($column,'=', $request['searchequal']);
                    }
                }
            }
    
            if($request['order']){
                foreach($request['order'] as $column){
                    if(!is_null($request['order_by'])) $query->orderBy($column,$request['order_by']);
                    else $query->orderBy($column,'asc');
                }
            }
    
    
            if($request['per_page'] && $request['page']){
                return $query->paginate($request['per_page'], ['*'], 'page', $request['page']);
            }

            return $query->get();
        });
        
    }

    function getById($id){
        $with = array();
        forEach($this->options as $option){
            array_push($with,$option);
        }
        if(sizeof($with) != 0){
            return $this->model->with($with)->find($id);
        }

        return $this->model->find($id);
    }

    public function save(Request $request){
        Cache::flush();

        $model     = new $this->modelName;
        $columns   = Schema::getColumnListing($model->getTable());
        $modelData = $request->all();

        $modelData = $this->beforeSaveAttribution($modelData);

        foreach($modelData as $key => $value){
            if(!in_array($key,$columns)){
                return response()->json(['message' => 'Coluna {'.$key.'} não existe'], 400);
            }
            $model[$key] = $value;
        }

        if(!$model->save()){
            return response()->json(['message' => 'Falha ao salvar registro'], 500);
        }
        $this->afterSaveAttribution($model);

        return response()->json(['message' => 'Registro salvo com sucesso!'], 200);
    }

    public function update(Request $request,$id){
        Cache::flush();

        $model     = $this->model->find($id);
        $columns   = Schema::getColumnListing($this->model->getTable());
        $modelData = $request->all();

        $modelData = $this->beforeUpdateAttribution($modelData,$model);

        foreach($modelData as $key => $value){
            if(!in_array($key,$columns)){
                return response()->json(['message' => 'Coluna {'.$key.'} não existe'], 400);
            }
            $model[$key] = $value;
        }

        if(!$model->save()){
            return response()->json(['message' => 'Falha ao atualizar registro'], 500);
        }

        $this->afterUpdateAttribution($model);

        return response()->json(['message' => 'Atualizado com sucesso!'], 200);
    }

    public function delete($id){
        Cache::flush();

        $model = $this->model->find($id);
        if($model && $this->softDeletes){
            $model->deleted_by = auth('api')->user()->email;
            $model->save();
        }

        if($model->delete()){
            return response()->json(['message' => 'Deletado com sucesso!'], 200);
        }
        return response()->json(['message' => 'Algo inesperado ocorreu!'], 500);
    }


    // ---------------------------------------------------------------------------------


    private function beforeSaveAttribution($modelData){
        if(in_array('brAddress',$this->options) && array_key_exists('br_address',$modelData)){
            BrAddressHandler::handleSaveBefore($modelData);
        }

        if(in_array('phones',$this->options) && array_key_exists('phones',$modelData)){
            PhoneHandler::handleSaveBefore($modelData);
        }

        if($this->addToBeforeAttribution){
            $modelData = $this->addToBeforeAttribution->__invoke($modelData);
        }

        return $modelData;
    }

    private function afterSaveAttribution($model){
        if(in_array('brAddress',$this->options)){
            BrAddressHandler::handleSaveAfter($model);
        }

        if(in_array('phones',$this->options)){
            PhoneHandler::handleSaveAfter($model);
        }

        if($this->addToAfterAttribution){
            $this->addToAfterAttribution->__invoke($model);
        }
    }

    private function beforeUpdateAttribution($modelData,$model){
        if(in_array('brAddress',$this->options)){
            BrAddressHandler::handleUpdateBefore($modelData);
        }

        if(in_array('phones',$this->options) && array_key_exists('phones',$modelData)){
            PhoneHandler::handleUpdateBefore($modelData);
        }

        if($this->addToBeforeAttribution){
            $modelData = $this->addToBeforeAttribution->__invoke($modelData);
        }

        return $modelData;
    }

    private function afterUpdateAttribution($model){
        if(in_array('brAddress',$this->options)){
            BrAddressHandler::handleUpdateAfter($model);
        }

        if(in_array('phones',$this->options)){
            PhoneHandler::handleUpdateAfter($model);
        }

        if($this->addToAfterAttribution){
            $this->addToAfterAttribution->__invoke($model);
        }
    }
}
