<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

interface DefaultCrudController
{
    public function list(Request $request);

    public function getById($id);

    public function save(Request $request);

    public function update(Request $request,$id);

    public function delete($id);

}
