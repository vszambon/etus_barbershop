<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;

use Illuminate\Http\Request;
use DB;

class AuthProxy
{
    public function attemptLogin($data) {
        $tokenRequest = Request::create(env('APP_URL').'/oauth/token','POST');
        return $this->response_handler(\Route::dispatch($tokenRequest)->getContent());
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function performLogout($token)
    {
        DB::table('oauth_refresh_tokens')->where('oauth_refresh_tokens.access_token_id','=',$token->id)->delete();
        $token->delete();
    }

    private function response_handler($response)
	{
		if ($response) {
			return json_decode($response);
		}
		return [];
	}

}
