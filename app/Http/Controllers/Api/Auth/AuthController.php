<?php

namespace App\Http\Controllers\Api\Auth;

use DB;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthController extends Controller
{
    use ThrottlesLogins;

    private $authproxy;

    public function __construct(AuthProxy $authproxy)
    {
        $this->authproxy = $authproxy;
    }
    public function maxAttempts(){
        //Lock out on 5th Login Attempt
        return 5;
    }

    public function decayMinutes(){
        //Lock for 1 minute
        return 1;
    }

    public function username(){
        return 'email';
    }

    public function signUp(Request $request){

        $request->validate([
            'name'     => 'required|string',
            'email'    => 'required|string|email|unique:users,email',
            'password' => 'required|string|confirmed'
        ]);

        $user = new User([
            'name'             => $request->name,
            'email'            => $request->email,
            'password'         => Hash::make($request->password,['rounds' => 4]),
        ]);

        $user->save();

        $user->roles()->attach(2);//register as user

        return response()->json([
            'message' => 'Successfully created user!',
        ], 201);
    }

    public function doLogin(Request $request) {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $request->validate([
            'email'    => 'required|string|email',
            'password' => 'required|string'
        ]);

        $userCredentials = request(['email','password']);

        // return response()->json(['message'=>$userCredentials],200);

        if(!auth()->attempt(['email' => $userCredentials['email'],'password'=>$userCredentials['password']])){
            $this->incrementLoginAttempts($request);
            return response(['message'=>'Email e/ou Password incorretos'],422);
        }
        else if(is_null(auth()->user()->email_verified_at)){
            return response(['message'=>'Por favor, valide seu e-mail através do email ativação'],401);
        }

        //Lazy Load roles
        $roles = auth()->user()->roles;

        $request->request->add([
            'username'      => $request->input('email'),
            'client_id'     => env('PASSWORD_CLIENT_ID'),
            'grant_type'    => 'password',
            'client_secret' => env('PASSWORD_CLIENT_SECRET'),
        ]);

        $this->clearLoginAttempts($request);
        return response()->json(['auth'=>$this->authproxy->attemptLogin($request->all()),'user'=>auth()->user()], 200);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $this->authproxy->performLogout(auth('api')->user()->token());
        return response()->json([
            'message' => 'Logged Out.',200
        ]);
    }

    public function check(Request $request){
        if(!is_null(auth('api')->user())){
            return response()->json(['user' => auth('api')->user()],200);
        }
        return response()->json(['user' => 'Unauthorized'],401);
    }

}
