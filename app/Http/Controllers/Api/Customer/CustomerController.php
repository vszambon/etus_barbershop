<?php

namespace App\Http\Controllers\Api\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\DefaultCrudControllerImpl;
use App\Http\Controllers\Api\DefaultCrudController;

use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\Cache;


class CustomerController extends Controller implements DefaultCrudController
{
    private $apiController;

    function __construct(){
        $this->apiController = new DefaultCrudControllerImpl('App\Models\Customer',['brAddress','phones'],true);
    }

    public function list(Request $request){
        return $this->apiController->list($request);
    }

    function getById($id){
        return $this->apiController->getById($id);
    }

    public function save(Request $request){
        $this->validateSaveRequest($request);
        return $this->apiController->save($request);
    }

    public function update(Request $request,$id){
        $this->validateUpdateRequest($request);
        return $this->apiController->update($request,$id);
    }

    public function delete($id){
        return $this->apiController->delete($id);
    }


    private function validateSaveRequest(Request $request){
        return $request->validate([

        ]);
    }

    private function validateUpdateRequest(Request $request){
        if(!is_null($request->input('email'))){
            $roles = auth('api')->user()->roles;
            foreach($roles as $role){
                if($role->id != 1 ){
                    $request->request->remove('email');
                }
            }
        }
        return $request->validate([

        ]);
    }

}
