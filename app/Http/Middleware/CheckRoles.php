<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,...$roles)
    {
        if(Auth::user()->hasAnyRoles($roles)){
            return $next($request);
        }
        return response()->json(['message' => 'User does not have the necessary role for this route.'], 401);
    }
}
