<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\RelationShips;

class Customer extends Model
{
    use RelationShips;

    protected $table= 'customers';

    public function appointments()
    {
        return $this->hasMany('App\Models\Appointment');
    }
}
