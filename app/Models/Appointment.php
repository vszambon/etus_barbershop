<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function services()
    {
        return $this->belongsToMany('App\Service', 'service_appointment', 'appointment_id', 'service_id');

    }
}
