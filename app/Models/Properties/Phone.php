<?php

namespace App\Models\Properties;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $table= 'phones';

    protected $hidden =[
        "has_phone_type",
        "has_phone_id",
        "created_at",
        "updated_at"
    ];

    public function hasPhone()
    {
        return $this->morphTo();
    }
}
