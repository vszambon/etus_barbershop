<?php

namespace App\Models\Properties;

use Illuminate\Database\Eloquent\Model;

class BrAddress extends Model
{
    protected $table = "br_address";

    protected $hidden =[
        "has_address_type",
        "has_address_id",
        "created_at",
        "updated_at"
    ];

    function hasAddress(){
        return $this->morphTo();
    }
}
