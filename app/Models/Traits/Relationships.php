<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Model;

use App\Models\Properties\BrAddress;
use App\Models\Properties\Phone;

trait RelationShips
{

    function brAddress(){
        return $this->morphMany(BrAddress::class, 'has_address');
    }

    function phones(){
        return $this->morphMany(Phone::class, 'has_phone');
    }

}
