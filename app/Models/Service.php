<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table= 'services';


    public function appointments()
    {
        return $this->belongsToMany('App\Appointment', 'service_appointment', 'service_id', 'appointment_id');
    }

}
