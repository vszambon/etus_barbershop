<?php

namespace App\Listeners;

use Laravel\Passport\Events\AccessTokenCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use DB;

class RevokeOldTokens
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AccessTokenCreated  $event
     * @return void
     */
    public function handle(AccessTokenCreated $event)
    {
        DB::table('oauth_access_tokens')->leftJoin('users','oauth_access_tokens.user_id','=','users.id')
        ->where(function($query) use($event){
            $query->where('oauth_access_tokens.id', '<>', $event->tokenId)
                ->where('oauth_access_tokens.user_id','=',auth()->id())
                ->whereNotNull('oauth_access_tokens.user_id');
        })
        ->orWhere(function($query){
            $query->where('oauth_access_tokens.expires_at','<',date('Y-n-d H:i:s'))
                ->whereNull('oauth_access_tokens.user_id');
        })
        ->delete();

        DB::table('oauth_refresh_tokens')->leftJoin('oauth_access_tokens','oauth_refresh_tokens.access_token_id','=','oauth_access_tokens.id')
        ->whereNull('oauth_access_tokens.id')->delete();
    }
}
