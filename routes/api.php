<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* Authorization */
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\Auth\AuthController@doLogin');
    Route::post('signup', 'Api\Auth\AuthController@signUp');
    Route::post('check', 'Api\Auth\AuthController@check');

    Route::middleware(['auth:api'])
    ->group( function() {
        Route::post('logout', 'Api\Auth\AuthController@logout');
    });

});
/* ------------ */

/* User */
Route::group([
    'prefix' => 'users'
], function () {

    Route::middleware(['auth:api'])
    ->group( function() {
        Route::get('', 'Api\User\UserController@list');
        Route::get('{id}', 'Api\User\UserController@getById');
    });

    Route::middleware(['auth:api','roles:admin'])
    ->group( function() {
        Route::post('', 'Api\User\UserController@save');
        Route::put('{id}', 'Api\User\UserController@update');
        Route::delete('{id}', 'Api\User\UserController@delete');
    });
});
/* -------------------------------------- */

/* User */
Route::group([
    'prefix' => 'customers'
], function () {

    Route::middleware(['auth:api'])
    ->group( function() {
        Route::get('', 'Api\Customer\CustomerController@list');
        Route::get('{id}', 'Api\Customer\CustomerController@getById');
    });

    Route::middleware(['auth:api','roles:admin'])
    ->group( function() {
        Route::post('', 'Api\Customer\CustomerController@save');
        Route::put('{id}', 'Api\Customer\CustomerController@update');
        Route::delete('{id}', 'Api\Customer\CustomerController@delete');
    });
});
/* -------------------------------------- */
