const mix = require('laravel-mix');

const JavaScriptObfuscator = require('webpack-obfuscator');
const {CleanWebpackPlugin}  = require('clean-webpack-plugin');



mix.disableNotifications();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false,
    })

    if(mix.inProduction()){
        console.log("IN PRODUCTION")
        mix.version();
        process.env.MIX_PRODUCTION = "true";
        mix.webpackConfig( webpack => {
            return {
                output: {
                    chunkFilename: 'js/chunks/[name].[chunkhash:6].js',
                    publicPath: process.env.MIX_PROJECT_PATH
                },
                plugins: [
                    new JavaScriptObfuscator ({
                        rotateUnicodeArray: true
                    }),
                    new CleanWebpackPlugin({
                        cleanOnceBeforeBuildPatterns: [
                            '**/*.js',
                        ],
                    })
                ],
            }
        })
    }
    else{
        process.env.MIX_PRODUCTION = "false";
        mix.webpackConfig( webpack => {
            return {
                output: {
                    chunkFilename: 'js/dev/chunks/[name].js',
                    publicPath: '/' //running with php artisan serve (http://localhost:8000)
                }
            }
        })
    }
