import Vue from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
    faEye,
    faBars,
    faHome,
    faLock,
    faEdit,
    faPlus,
    faTable,
    faPhone,
    faSearch,
    faQrcode,
    faCaretUp,
    faSpinner,
    faLockOpen,
    faEnvelope,
    faUserEdit,
    faTrashAlt,
    faDownload,
    faNewspaper,
    faCaretDown,
    faArrowLeft,
    faUserCircle,
    faSignOutAlt,
    faArrowRight,
    faPlusCircle,
    faStepForward,
    faTimesCircle,
    faCheckCircle,
    faStepBackward,
    faFileDownload,
    faAngleDoubleUp,
    faCloudUploadAlt,
    faAngleDoubleDown,
    faArrowCircleLeft,
} from '@fortawesome/free-solid-svg-icons'

import { //fab
    faYoutube,
    faLinkedin,
    faFacebookF,
    faInstagram,
} from '@fortawesome/free-brands-svg-icons'

import { //far
    faCalendarCheck,
    faNewspaper as farNewspaper,
} from '@fortawesome/free-regular-svg-icons'

library.add(
    //fas
    faEye,
    faBars,
    faHome,
    faLock,
    faEdit,
    faPlus,
    faTable,
    faPhone,
    faSearch,
    faQrcode,
    faCaretUp,
    faSpinner,
    faLockOpen,
    faEnvelope,
    faUserEdit,
    faTrashAlt,
    faNewspaper,
    faCaretDown,
    faArrowLeft,
    faUserCircle,
    faSignOutAlt,
    faArrowRight,
    faPlusCircle,
    faStepForward,
    faTimesCircle,
    faCheckCircle,
    faStepBackward,
    faFileDownload,
    faAngleDoubleUp,
    faCloudUploadAlt,
    faAngleDoubleDown,
    faArrowCircleLeft,
    faDownload,

    //fab
    faYoutube,
    faLinkedin,
    faFacebookF,
    faInstagram,

    //far
    farNewspaper,
    faCalendarCheck,
);

Vue.component('font-awesome-icon', FontAwesomeIcon)