import Vue from 'vue';
import VueRouter from 'vue-router';
import auth from './store/modules/authorization';
import CryptoJS from 'crypto-js';
import base from './store/modules/base_path' 


/* LAZY LOAD COMPONENTS */

//Referencia: const [name] = () =>
//                import ( /*webpackChunkName: "[name]" */ './components/views/[name]/[name].vue')

const login = () =>
    import( /*webpackChunkName: "login" */ './components/views/Auth/Login.vue')

const blankPage = () =>
    import( /*webpackChunkName: "blank" */ './components/views/BlankPage.vue')


/* ----------- */

/* Roles map used to 'protect' routes */
import {
    Role
} from './utils/Role' 
/* ---------------------------------- */


Vue.use(VueRouter);

export const router = new VueRouter({
    routes: [
        {
            path: '*',
            redirect: '/b' //<----- redirect non existent routes to default view
        },
        {
            path: '/b',
            component: blankPage,
            meta: {
                authorize: [Role.ADMIN]
            },
        },
        {
            path: '/401',
            component: login,
            meta: {
                authorize: []
            },
        },
        {
            path: '/login',
            component: login,
            meta: {
                authorize: []
            },
        },
    ],
    mode: 'history',
    base: base.state.baseURL,
    scrollBehavior (to, from, savedPosition) {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
});

var authRoutes = ['/login'];

router.beforeEach(async (to, from, next) => {

    const {
        authorize
    } = to.meta

    console.log(to.path)
    console.log(authorize.length)
    /* AUTHORIZATION CHEKS */
    if(authorize.length && (!auth.state.accessToken || !auth.state.roles)) {
        console.log("HERE")
        return next({
            path: '/login',
            query: {
                returnUrl: to.path
            }
        })
    }
    else if(authorize.length){
        console.log("THERE")
        var hash = CryptoJS.SHA256(process.env.APP_KEY).toString(CryptoJS.enc.Hex)
        auth.state.roles.forEach(role => {
            console.log(role)
            if (!authorize.includes(CryptoJS.AES.decrypt(role, hash).toString(CryptoJS.enc.Utf8)) && from.path != to.path) {
                console.log("401")
                return next({
                    path: '/401'
                });
            }
        });
    }

    // if authorized do not go to authorization pages
    if(authRoutes.includes(to.path) && auth.state.accessToken && auth.state.roles){
        console.log("EVERYWHERe")
        return next({
            path: '/b',
        })
    }

    if (from.path != to.path) { // && different from login routes /login;/signup ....
        next();
    }
})
