import path from './base_path'
import CryptoJS from 'crypto-js';

const state = {
    roles:        JSON.parse(localStorage.getItem('rls')) || null,
    header:       null,
    userData:     null,
    accessToken:  JSON.parse(localStorage.getItem('ptkn')) || null,
    errorMessage: '',
};

const getters = {
    getRoles:        (state) => state.roles,
    getHeader:       (state) => state.header,
    getUserData:     (state) => state.userData,
    getAccessToken:  (state) => state.accessToken,
    getErrorMessage: (state) => state.errorMessage,
};

const mutations = {
    setErrorMessage: (state,value) => {
        state.errorMessage = value
    },
    setUserData: (state, value) => {
        state.userData = value
    },
    setAccessToken: (state, value) => {
        state.accessToken = value
    },
    setHeader: (state, contentType) => {
        var authHeader = {}
        var hash = CryptoJS.SHA256(process.env.APP_KEY).toString(CryptoJS.enc.Hex)
        var token = state.accessToken ? CryptoJS.AES.decrypt(state.accessToken, hash).toString(CryptoJS.enc.Utf8) : '';
        authHeader = {
            'Authorization': 'Bearer ' + token
        }
        state.header = {
            headers: {
                ...authHeader,
                'content-type': contentType,
                'Accept': 'application/json',
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
                'X-Requested-With': "XMLHttpRequest"
            }
        }
    },
    clearAuthData: (state) => {
        state.roles = null
        state.userData = null
        state.accessToken = null
        state.errorMessage = null

        localStorage.removeItem('ptkn');
        localStorage.removeItem('rls');
    },
    SUCCESS_HANDLER: (state, response) => {
        state.errorMessage = ''

        var hash = CryptoJS.SHA256(process.env.APP_KEY).toString(CryptoJS.enc.Hex)

        state.accessToken = CryptoJS.AES.encrypt(response.data.auth.access_token, hash).toString();
        localStorage.setItem('ptkn', JSON.stringify(state.accessToken));

        let rls = [];
        response.data.user.roles.forEach(element => {
            rls.push(CryptoJS.AES.encrypt(element.name, hash).toString())
        });
        state.roles = rls;
        localStorage.setItem('rls',  JSON.stringify(rls));

        state.userData = response.data.user;
        state.successMessage = response.data.message
    },
};

const actions = {
    async doLogin({
        commit,
        state
    }, login) {

        state.errorMessage = ''

        commit('setHeader', 'application/json')
        const config = {
            ...state.header
        }

        return new Promise(async (resolve, reject) => {
            await axios.post(path.state.baseURL + '/api/auth/login', {
                    email: login.email,
                    password: login.password
            }, config)
            .then(response => {
                state.errorMessage = ''
                commit('SUCCESS_HANDLER', response);
                resolve(response)
            })
            .catch(err => {
                console.log(err)
                reject(err)
            })
        })
    },
    doLogout({
        commit,
        state
    }) {
        state.errorMessage = ''
        
        commit('setHeader', 'application/json')
        const config = {
            ...state.header
        }

        commit('clearAuthData');
        
        axios.post(path.state.baseURL + '/api/auth/logout', {}, config)
    },
    checkToken({
        commit,
        state,
    }) {
        commit('setHeader', 'application/json')
        const config = {
            ...state.header
        }
        console.log(config)
        return new Promise(async (resolve, reject) => {
            await axios.post(path.state.baseURL + '/api/auth/check', {}, config)
                .then(response => {
                    state.userData = response.data.user
                    console.log("SUCESSO CHECK'")
                    resolve(response);
                })
                .catch(err => {
                    console.log(err)
                    reject(err)
                })
        })
    }

};

export default {
    state,
    getters,
    mutations,
    actions
}
