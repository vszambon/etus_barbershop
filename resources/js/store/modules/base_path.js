const state = {
    baseURL: (JSON.parse(process.env.MIX_PRODUCTION)) ? process.env.MIX_BASE_URL : '',
};

const getters = {
    getBaseURL: (state) => state.baseURL,
};

const mutations = {

}

const actions = {

}

export default {
    state,
    getters,
    mutations,
    actions
}
