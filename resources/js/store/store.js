import Vuex from 'vuex';
import Vue from 'vue';

/* MODULES */
import authorization from './modules/authorization';
import basePath from './modules/base_path';
/* ------- */

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        authorization,
        basePath,
    }
});
