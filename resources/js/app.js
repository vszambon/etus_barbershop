
require('./bootstrap');

import { router } from './router';
import store from './store/store';
import fontawesome from './fontawesome';
import App from './App'; //Windows: ./App.vue

window.Vue = require('vue');

const app = new Vue({
    el: '#app',
    render: h => h(App),
    router,
    store
});

