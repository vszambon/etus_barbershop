<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="Description" content="Etus Teste - By: Vítor Santoro Zambon">
    <meta name="theme-color" content="#191d38" />

    <script defer src="{{ asset('js/app.js') }}"></script>

    <!-- icon in the highest resolution we need it for -->
    <link rel="icon" sizes="96x96" href="{{ asset('img/app/icon.png') }}">

    <title>Josias's BarberShop</title>

    <!-- Styles -->

    <style>
        html,
        body {
            overflow-x: hidden;
            font-family: Montserrat-Regular, sans-serif;
            font-display: auto;
            background-color: #eaf3fde6;
        }

        .override-background {
            display: block;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 99999999;
            /* Sit on top */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: black;
            opacity: 0.5;
        }
    </style>

    <script>
        function loadedBody() {
            ////console.log("body loaded!")
            setVisible('loading', false);
        }

        function setVisible(selector, visible) {
            document.getElementById(selector).style.display = visible ? 'block' : 'none';
        }
    </script>

    <!-- Scripts -->
    <script async type="text/javascript">
        /* First CSS File */
        var giftofspeed = document.createElement('link');
        giftofspeed.rel = 'stylesheet';
        giftofspeed.href = "{{ asset('css/app.css') }}";
        giftofspeed.type = 'text/css';
        var godefer = document.getElementsByTagName('link')[0];
        godefer.parentNode.insertBefore(giftofspeed, godefer);
    </script>

</head>

<body oncontextmenu="return false;" onload="loadedBody()">

    <div id="loading" class="override-background"></div>

    <div id="app" style="overflow: hidden;">
        <app></app>
    </div>
</body>


</html>
